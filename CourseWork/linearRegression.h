#pragma once
#include <vector>
#include <exception>
using namespace std;

class linearRegression
{
	//y = beta * x + alpha
	double alpha;
	double beta;
public:
	linearRegression();

	void getLinearRegression(vector<double> x, vector<double> y);

	linearRegression(vector<double> x, vector<double> y);

	double getError(double x, double y) const;

	double getValue(double x);

	~linearRegression(void);
};

