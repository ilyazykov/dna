#include "normalDistribution.h"

normalDistribution::normalDistribution()
{

}

normalDistribution::normalDistribution( vector<double> x, double expectation )
{
	this->expectation = expectation;

	double sum = 0;
	double sumsq = 0;
	for (int i = 0; i < x.size(); i++)
	{
		sum += x[i];
		sumsq += x[i]*x[i];
	}
	dispertion = (sumsq - sum*sum / x.size()) / (x.size() - 1);

	standartDeviation = sqrt(dispertion);
}

double normalDistribution::getZScore( double e )
{
	return e/standartDeviation;
}

normalDistribution::~normalDistribution( void )
{

}
