#include "ListGraph.h"


ListGraph::ListGraph( int n ) : vertexNumber(n), graph( list<int>(n))
{

}

ListGraph::~ListGraph( void )
{
	delete graph;
}

int ListGraph::CountVertex() const
{
	return vertexNumber;
}

void ListGraph::AddArc( int from, int to )
{
	if (from<0 || from>=vertexNumber || to<0 || to>=vertexNumber)
		return;
	graph[from].push_back(to);
}

bool ListGraph::HasArc( int from, int to ) const
{
	if (from<0 || from>=vertexNumber || to<0 || to>=vertexNumber)
		return false;

	list<int>::iterator iter;
	for (iter = graph[from].begin(); iter!=graph[from].end(); ++iter)
	{
		if (*iter == to) return true; 
	}
	return false;
}
