#pragma once
#include "graph.h"
#include <list>
using namespace std;

class ListGraph : public Graph
{
private:
	list<int> *graph;
	int vertexNumber;

public:
	ListGraph(int n);
	
	~ListGraph(void);

	int CountVertex() const;

	void AddArc(int from, int to);

	bool HasArc(int from, int to) const;
};

