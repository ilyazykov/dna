#pragma once
#include <vector>
#include <exception>
#include <iostream>
#include "linearRegression.h"
using namespace std;

class Human
{
	int numberMRA;

	vector<double> miRNAexpression; //TODO������ ���� ������ ������� � ����
	bool isSick;
	vector<vector<double> > errors;
public:
	

	Human();

	Human(int numberMRA);

	void setZScore(double value, int i, int j);

	double getError(int i, int j);

	void setErrors(const vector<vector<linearRegression> > & linReg, int xi, int yi);

	double getMiRNAexpression(int number)
	{
		return miRNAexpression[number];
	}

	void getHumanFromCSV();

	int getSizeMiRNAexpression();

	~Human(void);
};