#pragma once
#include "graph.h"
#include <vector>
#include <limits>
using namespace std;

class MatrixGraph : public Graph
{
	vector<vector<double> > matrix;
	int vertexNumber;
public:
	MatrixGraph();

	MatrixGraph(int vertexNumber);

	void create(int vertexNumber);

	~MatrixGraph(void){}
	
	int CountVertex() const;

	void AddArc(int from, int to, double value);

	bool HasArc(int from, int to) const;
	
	vector<vector<double> > getShortcuts() const; //Floyd�Warshall

	double getTopologicalMetrix();
};

